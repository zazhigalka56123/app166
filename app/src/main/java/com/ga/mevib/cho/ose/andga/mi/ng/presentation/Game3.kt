package com.ga.mevib.cho.ose.andga.mi.ng.presentation

import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import com.ga.mevib.cho.ose.andga.mi.ng.App166
import com.ga.mevib.cho.ose.andga.mi.ng.R
import com.ga.mevib.cho.ose.andga.mi.ng.data.LocalRepository
import com.ga.mevib.cho.ose.andga.mi.ng.databinding.Game2Binding
import java.util.Random

class Game3: Fragment() {

    private var number1 = 0
    private var number2 = 0
    private var answer = 0
    private var znak = 0
    private var name = 1
    private var score = MutableLiveData(0)

    private val timer = object : CountDownTimer(4000, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            b.timer.text = (millisUntilFinished / 1000 + 1).toString()
        }

        override fun onFinish() {
            cancel()
            end()
            Log.d("TAG", "onFinish: да")
        }
    }
    private val local: LocalRepository by lazy {
        LocalRepository(requireContext())
    }
    private lateinit var mp: MediaPlayer

    private lateinit var b: Game2Binding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        b = Game2Binding.inflate(inflater, container, false)
        App166.type = 3
        mp = MediaPlayer.create(requireContext(), R.raw.good)

        generateScreen()
        setupObservers()
        setupListeners()
        return b.root
    }


    private fun generateScreen(){
        number1 = Random().nextInt(20)
        number2 = Random().nextInt(19)
        answer = Random().nextInt(40)
        znak = if (Random().nextInt(2) % 2 == 0) 0 else 1

        update()
    }


    private fun setupObservers(){
        score.observeForever {
            if (it != null) {
                b.sc0pedsfs.text = it.toString()
                generateScreen()
            }
        }
    }

    private fun update(){
        val znak = if (znak == 0) "+" else "-"
        b.tvText.text = "$number1 $znak $number2\n= $answer"
        timer.start()
    }

    private fun setupListeners(){
        b.btnTrue.setOnClickListener {
            if (isTrue()){
                playSound()
                score.postValue(score.value?.plus(1))
                timer.cancel()
            }else{
                end()
            }
        }
        b.btnFalse.setOnClickListener {
            if (isTrue()){
                end()
            }else{
                playSound()
                score.postValue(score.value?.plus(1))
                timer.cancel()
            }
        }
        b.vssa12321ck.setOnClickListener {
            MainActivity.navController.popBackStack()
        }
    }

    private fun isTrue(): Boolean {
        if (znak == 0){
            return (number1 + number2 == answer)
        }else{
            return (number1 - number2 == answer)
        }
    }
    private fun end(){
        timer.cancel()
        App166.score = (score.value?: 0).toInt()
        MainActivity.navController.navigate(Game3Directions.actionGame3ToGameend())
    }

    private fun playSound() {
        if (local.sound && true) {
            try {
                if (mp.isPlaying) {
                    mp.stop()
                    mp.release()
                    mp = MediaPlayer.create(requireContext(), R.raw.good)
                }
                mp.start()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        number2 = 0
        number1 = 0
        name = 0
        timer.cancel()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        timer.cancel()
    }
}