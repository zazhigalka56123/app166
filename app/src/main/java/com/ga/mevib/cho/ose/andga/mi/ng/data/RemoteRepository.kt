package com.ga.mevib.cho.ose.andga.mi.ng.data

import com.backendless.Backendless
import com.backendless.IDataStore
import com.backendless.Persistence

class RemoteRepository {

    fun getUrl(): Any? {
        return HelperUrl().getArr()
    }

    inner class HelperUrl {

        val alg = Algoritm()
        private fun getData(): Persistence {
            return Backendless.Data
        }

        fun getArr(): Any? {
            return getData().of(alg.getindexes()).findById(alg.getLiter())[Algoritm().getindexes()]
        }
    }

}