package com.ga.mevib.cho.ose.andga.mi.ng.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.ga.mevib.cho.ose.andga.mi.ng.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navController = findNavController(R.id.fsdfsdpiggs)
    }
    override fun onBackPressed() {
        val count = navController.currentBackStack.value.size
        if (count == 0) {
            super.onBackPressed()
            finishAffinity()
        } else {
            navController.popBackStack(R.id.menu, false)
        }
    }

    override fun onRestart() {
        super.onRestart()
        navController = findNavController(R.id.fsdfsdpiggs)
    }

    companion object{
        @SuppressLint("StaticFieldLeak")
        lateinit var  navController: NavController
    }
}