package com.ga.mevib.cho.ose.andga.mi.ng.presentation

import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import com.ga.mevib.cho.ose.andga.mi.ng.App166
import com.ga.mevib.cho.ose.andga.mi.ng.R
import com.ga.mevib.cho.ose.andga.mi.ng.data.LocalRepository
import com.ga.mevib.cho.ose.andga.mi.ng.databinding.Game1Binding
import java.util.Random


class Game1: Fragment() {

    private var color = MutableLiveData(-1)
    private var name = MutableLiveData(-1)
    private var score = MutableLiveData(0)

    private val colors = listOf(R.color.c_red, R.color.c_blue, R.color.c_green, R.color.c_yellow, R.color.c_orange, R.color.c_purple, R.color.c_black)
    private val names = listOf(R.string.c_red, R.string.c_blue, R.string.c_green, R.string.c_yellow, R.string.c_orange, R.string.c_purple, R.string.c_black)

    private lateinit var mp: MediaPlayer
    private val timer = object : CountDownTimer(4000, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            b.timer.text = (millisUntilFinished / 1000 + 1).toString()
        }

        override fun onFinish() {
            cancel()
            App166.score = (score.value?: 0).toInt()
            MainActivity.navController.navigate(Game1Directions.actionGame1ToGameend())
        }
    }
    private val local: LocalRepository by lazy {
        LocalRepository(requireContext())
    }
    private lateinit var b: Game1Binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        b = Game1Binding.inflate(inflater, container, false)

        mp = MediaPlayer.create(requireContext(), R.raw.good)




        App166.type = 1

        generateScreen()
        setupObservers()
        setupListeners()
        return b.root
    }


    private fun generateScreen(){
        color.postValue(Random().nextInt(7))
        name.postValue(Random().nextInt(7))
        timer.start()
    }


    private fun setupObservers(){
        color.observeForever {
            if (it != -1 && it != null)
                b.tec42fds.setBackgroundColor(requireContext().getColor(colors[it]))
        }
        name.observeForever {
            if (it != -1 && it != null)
              b.tvName.text = getString(names[it])
        }
        score.observeForever {
            if (it != null) {
                b.tvScore.text = it.toString()
                generateScreen()
            }
        }
    }

    private fun setupListeners(){
        b.btnTrue.setOnClickListener {
            if (isTrue()){
                playSound()
                score.postValue(score.value?.plus(1))
                timer.cancel()
            }else{
                end()
            }
        }
        b.btnFalse.setOnClickListener {
            if (isTrue()){
                end()
            }else{
                playSound()
                score.postValue(score.value?.plus(1))
                timer.cancel()
            }
        }
        b.zfefsdlasdasf.setOnClickListener {
            MainActivity.navController.popBackStack()
        }


    }

    private fun isTrue() = (color.value == name.value && color.value != null)

    private fun end(){
        timer.cancel()
        App166.score = (score.value?: 0).toInt()
        MainActivity.navController.navigate(Game1Directions.actionGame1ToGameend())
    }
    private fun playSound() {
        if (local.sound && true) {
            try {
                if (mp.isPlaying) {
                    mp.stop()
                    mp.release()
                    mp = MediaPlayer.create(requireContext(), R.raw.good)
                }
                mp.start()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        timer.cancel()
        isTrue()
        colors[0]
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (isTrue())
            timer.cancel()
        else
            timer.cancel()
    }

}