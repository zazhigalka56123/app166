package com.ga.mevib.cho.ose.andga.mi.ng.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.ga.mevib.cho.ose.andga.mi.ng.data.LocalRepository
import com.ga.mevib.cho.ose.andga.mi.ng.databinding.SettingsBinding

class Settngs: Fragment(){

    private var stateSound = true
    private var stateVolume = true

    private lateinit var b: SettingsBinding
    private val db by lazy{
        LocalRepository(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        b = SettingsBinding.inflate(inflater, container, false)

        stateSound = db.sound
        stateVolume = true

        update()

        b.sounds.setOnClickListener {
            stateSound = !stateSound
            update()
        }


        b.zfefsdlasdasf.setOnClickListener {
            MainActivity.navController.popBackStack()
        }
        return b.root
    }


    private fun update(){
        if (stateSound) {
            b.sounds.text = "Sounds: ON"
        }else{
            b.sounds.text = "Sounds: OFF"
        }
        db.setSounds(stateSound)

    }

}