package com.ga.mevib.cho.ose.andga.mi.ng.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.ga.mevib.cho.ose.andga.mi.ng.R
import com.ga.mevib.cho.ose.andga.mi.ng.databinding.TriMedvedyaBinding

class Choose: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val b = TriMedvedyaBinding.inflate(inflater, container, false)

        b.loolltt.playAnimation()

        b.br43.setOnClickListener {
            MainActivity.navController.popBackStack()
        }

        b.cardColors.setOnClickListener {
            MainActivity.navController.navigate(ChooseDirections.actionMenuToGame1())
        }

        b.cardArrows.setOnClickListener {
            MainActivity.navController.navigate(ChooseDirections.actionMenuToGame2())
        }

        b.cardNumbers.setOnClickListener {
            MainActivity.navController.navigate(ChooseDirections.actionChooseToGame3())
        }

        return b.root
    }
}