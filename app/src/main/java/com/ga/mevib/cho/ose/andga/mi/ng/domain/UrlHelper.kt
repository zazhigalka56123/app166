package com.ga.mevib.cho.ose.andga.mi.ng.domain

import java.net.HttpURLConnection
import java.net.URL

class UrlHelper {
    fun endPoint(url: String?): String? {
        try {
            val fsfdsfdsfsd = (URL(url).openConnection() as HttpURLConnection).apply {
                instanceFollowRedirects = false
                connect()
            }
            if (fsfdsfdsfsd.responseCode == HttpURLConnection.HTTP_MOVED_PERM) {
                val fdsfdsfds = fsfdsfdsfsd.getHeaderField("Location")
                return endPoint(fdsfdsfds)
            }
            if (fsfdsfdsfsd.responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                return endPoint(fsfdsfdsfsd.getHeaderField("Location"))
            }
        } catch (e: Exception) {
            return url
        }
        return url
    }
}