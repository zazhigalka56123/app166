package com.ga.mevib.cho.ose.andga.mi.ng.presentation

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.ga.mevib.cho.ose.andga.mi.ng.R
import com.ga.mevib.cho.ose.andga.mi.ng.data.LocalRepository
import com.ga.mevib.cho.ose.andga.mi.ng.databinding.SplashBinding
import com.ga.mevib.cho.ose.andga.mi.ng.domain.Authorization
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class Splash: Fragment() {

    private val db by lazy {
        LocalRepository(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val b = SplashBinding.inflate(inflater, container, false)
        Glide.with(requireActivity()).load(R.drawable.img_back_spl).into(b.ivvvsd)


        if (db.getLinkIsSaved()){
            val link = db.link
            println("!db: ${db.link}")
            lifecycleScope.launch(Dispatchers.IO) {
                navigate(link != "null" && link != "")
            }
        }else {
            lifecycleScope.launch(Dispatchers.IO) {
                val ans = Authorization().where2go(requireContext())
                println("ansss $ans")
                navigate(ans)
            }
        }

        return b.root
    }

    private suspend fun navigate(flag: Boolean){
        withContext(Dispatchers.Main) {
            if (flag) {
                println("navigate")
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(db.link)
                try {
                    requireActivity().startActivity(i)
                }catch (e: Exception){
                    Toast.makeText(requireContext(), "Произошла ошибка, попробуйте позже", Toast.LENGTH_SHORT).show()
                    db.setLinkSaved(false)
                }
            } else {
                println("nav menu $flag")
                MainActivity.navController.navigate(R.id.menu)
            }
        }
    }
}