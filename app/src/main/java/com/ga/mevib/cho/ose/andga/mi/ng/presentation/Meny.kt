package com.ga.mevib.cho.ose.andga.mi.ng.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.ga.mevib.cho.ose.andga.mi.ng.R
import com.ga.mevib.cho.ose.andga.mi.ng.databinding.MenuBinding

class Meny: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val b = MenuBinding.inflate(inflater, container, false)

        b.startbtn.setOnClickListener {
            MainActivity.navController.navigate(R.id.choose)
        }
        b.settbtn.setOnClickListener {
            MainActivity.navController.navigate(R.id.qweswer)
        }


        return b.root
    }
}