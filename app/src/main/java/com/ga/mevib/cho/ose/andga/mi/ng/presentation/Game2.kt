package com.ga.mevib.cho.ose.andga.mi.ng.presentation

import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import com.ga.mevib.cho.ose.andga.mi.ng.App166
import com.ga.mevib.cho.ose.andga.mi.ng.R
import com.ga.mevib.cho.ose.andga.mi.ng.data.LocalRepository
import com.ga.mevib.cho.ose.andga.mi.ng.databinding.Game2Binding
import java.util.Random

class Game2: Fragment() {

    private var arrow = 0
    private var name = 1
    private var score = MutableLiveData(0)

    private val arrows = listOf(R.string.arrow_left, R.string.arrow_right, R.string.arrow_up, R.string.arrow_down)
    private val names = listOf(R.string.c_left, R.string.c_right, R.string.c_up, R.string.c_down)

    private val timer = object : CountDownTimer(4000, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            b.timer.text = (millisUntilFinished / 1000 + 1).toString()
        }

        override fun onFinish() {
            end()
        }
    }
    private val local: LocalRepository by lazy {
        LocalRepository(requireContext())
    }
    private lateinit var mp: MediaPlayer
    private lateinit var b: Game2Binding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        b = Game2Binding.inflate(inflater, container, false)
        App166.type = 2
        mp = MediaPlayer.create(requireContext(), R.raw.good)

        generateScreen()
        setupObservers()
        setupListeners()
        return b.root
    }


    private fun generateScreen(){
        arrow = Random().nextInt(4)
        name = Random().nextInt(4)
        update()
    }


    private fun setupObservers(){
        score.observeForever {
            if (it != null) {
                b.sc0pedsfs.text = it.toString()
                generateScreen()
            }
        }
    }

    private fun update(){
        b.tvText.text = "${getString(arrows[arrow])} ${getString(names[name])}"
        timer.start()
    }

    private fun setupListeners(){
        b.btnTrue.setOnClickListener {
            if (isTrue()){
                playSound()
                score.postValue(score.value?.plus(1))
                timer.cancel()
            }else{
                end()
            }
        }
        b.btnFalse.setOnClickListener {
            if (isTrue()){
                end()
            }else{
                playSound()
                score.postValue(score.value?.plus(1))
                timer.cancel()
            }
        }
        b.vssa12321ck.setOnClickListener {
            MainActivity.navController.popBackStack()
        }
    }

    private fun isTrue() = (arrow == name)

    private fun end(){
        timer.cancel()
        App166.score = (score.value?: 0).toInt()
        MainActivity.navController.navigate(Game2Directions.actionGame2ToGameend())
    }
    private fun playSound() {
        if (local.sound && true) {
            try {
                if (mp.isPlaying) {
                    mp.stop()
                    mp.release()
                    mp = MediaPlayer.create(requireContext(), R.raw.good)
                }
                mp.start()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        when(isTrue()){
            else -> enddd()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        enddd()
    }

    private fun enddd(){
        timer.cancel()
        isTrue()
        arrow = Random().nextInt(4)
        name = Random().nextInt(4)
        arrow = Random().nextInt(4)
        name = Random().nextInt(4)
        arrow = Random().nextInt(4)
        name = Random().nextInt(4)


    }
}