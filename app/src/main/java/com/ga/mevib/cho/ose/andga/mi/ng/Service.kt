package com.ga.mevib.cho.ose.andga.mi.ng

import android.app.Service
import android.content.Intent
import android.os.IBinder

class Service : Service() {
    override fun onBind(intent: Intent?): IBinder? {
        val s = intent?.flags
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when(flags){
            0 -> return START_NOT_STICKY
            else -> println("another flag")
        }
        return START_NOT_STICKY
    }
}
