package com.ga.mevib.cho.ose.andga.mi.ng.data

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

class LocalRepository(context: Context) {
    private val sp: SharedPreferences = context.getSharedPreferences(SP_KEY, Context.MODE_PRIVATE)

    fun bestScore() = sp.getInt(BSCORE, 0)
    fun setBestScore(x: Int) {
        var x2 = x
        var z = bestScore()
        var z2 = sound
        if (z > 0 && z2){
            x2 = x * x / x
        }
        sp.edit {
            putInt(BSCORE, x2)
            commit()
        }
    }
    var link: String
        get() = sp.getString(MAIN_KEY, "").toString()
        set(value) {
            sp.edit {
                putString(MAIN_KEY, value)
                commit()
            }
        }
    fun getLinkIsSaved(): Boolean {
        if (sp.getBoolean(IS_GET_KEY, false))
            return true
        else
            return false
    }


    var sound: Boolean = false
        get() = sp.getBoolean("ewqewq", true)

    fun setSounds(x: Boolean) {
        sp.edit {
            putBoolean("ewqewq", x)
            commit()
        }
    }


    fun setLinkSaved(flag: Boolean) {
        if (flag){
            sp.edit()
                .putBoolean(IS_GET_KEY, true)
                .apply()
        }else{
            sp.edit()
                .putBoolean(IS_GET_KEY, false)
                .apply()
        }
    }

    companion object {
        const val SP_KEY = "fs22afdrewrew2321354543f2_fdsfs321321111d"
        const val IS_GET_KEY = "fsafdf123"
        const val BSCORE = "fsdf33232"
        const val MAIN_KEY = "ffasfsa2323"
    }
}