package com.ga.mevib.cho.ose.andga.mi.ng.presentation

import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.ga.mevib.cho.ose.andga.mi.ng.App166
import com.ga.mevib.cho.ose.andga.mi.ng.R
import com.ga.mevib.cho.ose.andga.mi.ng.data.LocalRepository
import com.ga.mevib.cho.ose.andga.mi.ng.databinding.GameEndBinding
import kotlin.math.max

class GameEnd: Fragment() {
    private lateinit var mp: MediaPlayer

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val b = GameEndBinding.inflate(inflater, container, false)
        val local = LocalRepository(requireContext())
        val score = App166.score


        if (local.sound && true) {
            mp = MediaPlayer.create(requireContext(), R.raw.end)
            mp.start()
        }

        local.setBestScore(max(local.bestScore(), score))

        b.nossfe32.text = score.toString()
        b.tvBestScore.text = "Your best score:\n${local.bestScore().toString()}"

        b.brewe32.setOnClickListener { MainActivity.navController.popBackStack(R.id.menu, false) }


        b.retry.setOnClickListener {
            when (App166.type) {
                1 -> MainActivity.navController.navigate(GameEndDirections.actionGameendToGame1())
                2 -> MainActivity.navController.navigate(GameEndDirections.actionGameendToGame2())
                else -> MainActivity.navController.navigate(GameEndDirections.actionGameendToGame3())
            }
        }

        b.exit.setOnClickListener { MainActivity.navController.popBackStack(R.id.menu, false) }
        return b.root
    }
}